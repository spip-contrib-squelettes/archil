# ![](/images/archil-logo.svg?=100x100) Archil

Squelette mono-utilisateur pour SPIP

 - Documentation : [contrib.spip.net/Squelette-Archil](https://contrib.spip.net/Squelette-Archil)
 - Site de démonstration : [archil.infini.fr](https://archil.infini.fr)