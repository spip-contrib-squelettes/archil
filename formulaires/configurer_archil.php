<?php
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}
/**
 * Un simple formulaire de config,
 * on a juste à déclarer les saisies.
 * @return array
 **/
function formulaires_configurer_archil_saisies(): array {
	// $saisies est un tableau décrivant les saisies à afficher dans le formulaire de configuration
	$saisies = [
		[	// CONFIGURATION GÉNÉRALE
			'saisie' => 'fieldset',
			'options' => [
				'nom' => 'cfg_theme_titre',
				'label' => '<:archil:cfg_theme_titre:>'
			],
			'saisies' =>	[
			[	// Couleur principale de mise en valeur
				'saisie' => 'couleur',
				'options' => [
					'nom' => 'cfg_theme_clr_principal',
					'label' => '<:archil:cfg_theme_clr_principal:>',
					'defaut' => '#FF0000',
					'maxlength' => '7'
				],
				'verifier' => [
					'type' => 'couleur',
					'options' => [
						'type' => 'hexa',
						'normaliser' => 'oui'
						]
					]
			],
			[	// Couleur de la police
				'saisie' => 'couleur',
				'options' => [
					'nom' => 'cfg_theme_clr_police',
					'label' => '<:archil:cfg_theme_clr_police:>',
					'defaut' => '#232333',
					'maxlength' => '7'
				],
				'verifier' => [
					'type' => 'couleur',
					'options' => [
						'type' => 'hexa',
						'normaliser' => 'oui'
						]
					]
			],
			[	// Couleur de la police au survol
				'saisie' => 'couleur',
				'options' => [
					'nom' => 'cfg_theme_clr_police_survol',
					'label' => '<:archil:cfg_theme_clr_police_survol:>',
					'defaut' => '#FFFFFF',
					'maxlength' => '7'
				],
				'verifier' => [
					'type' => 'couleur',
					'options' => [
						'type' => 'hexa',
						'normaliser' => 'oui'
						]
					]
			],
			[	// Couleur des lignes
				'saisie' => 'couleur',
				'options' => [
					'nom' => 'cfg_theme_clr_lignes',
					'label' => '<:archil:cfg_theme_clr_lignes:>',
					'defaut' => '#FFFFFF',
					'maxlength' => '7'
				],
				'verifier' => [
					'type' => 'couleur',
					'options' => [
						'type' => 'hexa',
						'normaliser' => 'oui'
						]
					]
				]
			]
		],
		[	// BANNIÈRE & MENU
			'saisie' => 'fieldset',
			'options' => [ // Titre Bannière & menu
				'nom' => 'cfg_bandeau_titre',
				'label' => '<:archil:cfg_bandeau_titre:>'
				],
			'saisies' =>	[
			[	// Séléctions des articles et rubriques
				'saisie' => 'selecteur_rubrique_article',
				'options' => [
					'nom' => 'menu_articles',
					'multiple' => 'oui',
					'label' => '<:archil:cfg_bandeau_artrub:>',
					'explication' => '<:archil:cfg_bandeau_artrub_info:>',
					]
				]
			]
		],
		[	// PIED DE PAGE
			'saisie' => 'fieldset',
			'options' => [
				'nom' => 'cfg_footer_titre',
				'label' => '<:archil:cfg_footer_titre:>'
				],
			'saisies' =>	[
			[	// Séléction des articles à ajouter en pied de page
				'saisie' => 'selecteur_article',
				'options' => [
					'nom' => 'footer_articles',
					'multiple' => 'oui',
					'label' => '<:archil:cfg_footer_articles:>',
					'explication' => '<:archil:cfg_footer_articles_info:>',
					]
				]
			]
		]
	];
	return $saisies;
}


