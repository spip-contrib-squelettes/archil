<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(
//
// spip-core
//
'accueil_site' => 'Accueil',
// C
// cfg_ elements du formulaire de configuration d'archil
'cfg_archil_menu' => 'Configurer le squelette Archil',
'cfg_archil_titre' => 'Configuration Archil',
'cfg_couleurs' => 'Thème de couleurs',
'cfg_bandeau_titre' => 'Bannière et menu',
'cfg_bandeau_artrub' => 'Article ou rubrique à insérer dans le menu',
'cfg_bandeau_artrub_info' => 'Rubriques et articles qui constituent le menu. Limiter le nombre à 5 maximum',
'cfg_footer_titre' => 'Pied de page',
'cfg_footer_articles' => 'Choix des articles à afficher dans le pied de page',
'cfg_footer_articles_info' => ' Ajouter les liens techniques (crédits, mentions légales, ...)',
'cfg_theme_titre' => 'Configuration générale',
'cfg_theme_clr_principal' => 'Couleur principale',
'cfg_theme_clr_police' => 'Couleur police',
'cfg_theme_clr_police_survol' => 'Couleur police (Survol)',
'cfg_theme_clr_lignes' => 'Couleur lignes',
// H
'habillage_par' => 'Déguisé avec ',
// L
'lire_la_suite' => 'Lire la suite →',
//M
'mots_cles' => 'Tags',
// P
'publie_le' => 'Publié le',
'propulse_par' => 'Catapulté par ',
// T
'titre_tags' => 'Tags',
);