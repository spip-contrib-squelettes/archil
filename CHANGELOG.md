# Changelog

## 1.2.1 - 2024-10-10

### Added
 - Le squelette des tags affiche le logo tags à la place du # en titre de niveau 1 806e19e05aaebfbe0932b06ee410d46f219f7cb9
 - L'affichage des tags a été amélioré au survol  8d00ad32b50532d8c8c7c7538a5cc6ef8135310c

### Changed
 - Réécriture de la page de configuration
 - Réduction de l'espace inutilisé dans le header

 ### Fixed
 - #25 Les titres lorsqu'ils sont sur deux lignes ne se chevauchent plus
 
## 1.2.0 - 2024-07-05

### Changed
 - Compatibilité maximum 4

## 1.1.3 - 2023-09-26

### Fixed
 - Bug d'affichage du menu
 
## 1.1.2 - 2023-09-26

### Added
 - Possibilité d'ajouter des liens vers des articles en pied de page
 - Intégration et affichae des mots-clés

### Changed
- Il faut sélectionner les rubriques ou articles à afficher dans le menu

## 1.1.1 - 2023-09-24

### Added
 - #10 Ajout d'un CHANGELOG.md
 - Ajout d'un README.md

## Changed
 - Le plugin **Sociaux** n'est plus nécessaire mais seulement utilisé
 - #9 Utilisation des déclaration de saisies en PHP dans le formulaire de configuration

## Removed
 - #6 Suppression de la possibilité de choisir la couleur de survol des liens

### Fixed
 - #5 La page de configuration à un titre
 - Les couleurs par défaut respect les recommandations d'accessiblité en terme de contrast
 
## 1.1.0 - 2023-08-15

## Added
 - #2 Ajout d'une page de configuration pour Archil
 - Changement possible du jeu de couleurs du squelette